Breaking changes in 0.2:
Signature for `Terminal.__init__` has changed. connectivity_node has moved up in the parameter list as phases now defaults to `TracedPhases()` with a phase of `PhaseCode.ABCN`
